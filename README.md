1 The pickle file patients.p saves the list of the the absolute paths for all images.

![patinents](/images/patients.PNG)


2 The pickle file laebl.p saves the global labels for all images.

![labels](/images/labels.PNG)

3 The 3D images with format npy are obtaind from stacking slices of one specific phase in the right order together.

 The code transfer slices to 3D images and the code for plotting 3D images could be found here.

 https://gitlab.com/litiyao/nako_dataset_processing/tree/master/4D_array_with_norm
 
 If the slices are in right order, the plot of 3D imgaes should look like this.
 
 ![class](/images/class1.png)


4 Instruction for using this program
  1) transfer slices to 3D images in format npy using scripts here https://gitlab.com/litiyao/nako_dataset_processing/tree/master/4D_array_with_norm.
  2) get corresponding patinents.p and labels.p and save them in the home directory
  3) run demo_train, the parameters for training could be changed in demo_train
  4) after getting models and histories, models could be evaluated and the histories could be plotted with scripts in folder eval_models
  

