import numpy as np
import pandas as pd
import pandas
import ntpath
import os
import pickle


class Patient():
    """Creates patient."""

    def __init__(self, dicom_dirs, label_dirs, forget_slices=False):
        """"Constructor."""
        self.dicom_dirs = dicom_dirs  # dicom array path
        self.label_dirs = label_dirs # 已经是 1,2,3,
        self.slices = None
        self.shape = None
        self.prediction = None
        self.forget_slices = forget_slices
        self.slice_counter = 0

    def get_slices(self, count=True, verbose=False):
        """Take loaded slices if already available or load slices if not."""
        if count:  # 如果count是true， 计数加一
            self.slice_counter += 1
        if self.slices is None:
            self.slices = self.load_slices(verbose=verbose)
            # print("slice-counter: " + str(self.slice_counter))
            # print("load slices!")
        return self.slices

    def drop(self):
        """Drop slices for PatientBuffer in training."""
        # if self.slices == None:
        # print("Slices are already None!")
        if self.slice_counter != 0:  # 如果slice计数不等于0，计数减去1
            self.slice_counter -= 1
            if self.slice_counter == 0 and self.forget_slices:
                self.slices = None

    def load_slices(self, verbose=False):
        """Load patients data (dicom- and nii-files) in numpy arrays."""
        ArrayDicom = np.load(self.dicom_dirs)
        label =  self.label_dirs

        '''basename = os.path.basename("D:/2channel_array_with_norm/100063_30.npy")
        basename = os.path.basename(self.dicom_dirs)
        patientID = basename[:9]

        Label_dataframe = pd.read_excel(self.label_dirs)
        #Label_dataframe = pd.read_excel('C:/Users/litiy/PycharmProjects/FCN_classification_Tiyao/CombiExcel.xlsx')
        patientID_column = Label_dataframe['PatientID_FolderName']
        NumberOfRowsNE = len(Label_dataframe.index)
        for RowInNE in range(NumberOfRowsNE):
            row1 = patientID_column.iloc[RowInNE]
            if row1 == patientID:
                Label = Label_dataframe.at[RowInNE, 'QualityRating']
                #print(RowInNE) '''

        return (ArrayDicom, label)


class ShiftRegister():
    """Creates shift register for "first in first out queue" purpose."""

    def __init__(self, capacity):
        self.capacity = capacity # 30
        self.content = capacity * [0] # 有30个0的list <class 'list'>: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        self.pointer = 0 # 指针

    def shift(self, new_data, verbose=False):
        # if verbose:
        # print(" Fill PatientBuffer. Shifting: ",self.pointer)
        self.content[self.pointer] = new_data # 数据给指针指向的内容  content[0] = patient0 content[1] = patient1 content 包含30个patient 对象
        self.pointer = (self.pointer + 1) % self.capacity # 0, 1, 2 这样一直往后指 指针向后移动1

    def read(self):
        return self.content # 返回数据


def split(k, i, lst, perc):
    idxs = set(range(k)) - {i} #{0, 1, 2, 3} - {i}
    ys = [lst[j] for j in idxs]
    result = []
    for y in ys:
        result += y[0:int(len(y)*perc)] #add sublist together as train
    return (lst[i], result) # 验证集 list， train 集 list

def load_correct_patients(path,
                          patients_to_take,
                          forget_slices=False,
                          cut=None,
                          k=None,
                          perc=0.25,
                          iteration=None,
                          last_val_patients=None,
                          verbose=False):
    """Load valid patient-paths from hard disk and generate patient-objects."""
    # 测试集 45 个病人
    with open(os.path.join(path, 'patients.p'), 'rb') as patients:
        DicomPathsList = pickle.load(patients)  # dicom path list
    with open(os.path.join(path, 'labels.p'), 'rb') as labels:  # NiiPathsList?
        NiiPathsList = pickle.load(labels)  # like txt better

    # take patients for testing
    test_paths_dcm = DicomPathsList[:patients_to_take]  # first patients_to_take
    test_paths_nii = NiiPathsList[:patients_to_take]  # first 45 labels

    # 训练集取剩下的
    DicomPathsList = DicomPathsList[patients_to_take:]  # rest for train
    NiiPathsList = NiiPathsList[patients_to_take:]  # rest for train

    # 训练集病人数量
    patients_num = len(DicomPathsList)  # number of training data
    # 如果k在的话 k folder cross-validation
    if k is not None:
        # k-fold cross-validation
        steps = int(patients_num / k)  # 病人数除以折数然后取整
        ks_dicom = [DicomPathsList[(i * steps):((i * steps) + steps)] for i in
                    range(k)]  # train data into k folders 扔掉没法整除的后几个 一组steps个 k组，得到k folders
        ks_nii = [NiiPathsList[(i * steps):((i * steps) + steps)] for i in range(k)]  # 标签也分为 4个folders
        val_paths_dcm, train_paths_dcm = split(k=k, i=iteration, lst=ks_dicom,
                                               perc=perc)  # 第 i 次得到相应 val date 和 train data
        val_paths_nii, train_paths_nii = split(k=k, i=iteration, lst=ks_nii,
                                               perc=perc)  # 第 i 次得到相应 val label date 和 train label data

    else:
        # cut into validation- and training-data
        cut = int(cut * patients_num)  # 正常的 验证集和训练集分割， 非K folder
        val_paths_dcm = DicomPathsList[:cut]  # 验证集输入
        val_paths_nii = NiiPathsList[:cut]  # 验证集label
        train_paths_dcm = DicomPathsList[cut:]  # train集输入
        train_paths_nii = NiiPathsList[cut:]  # train集label

    # drop last validation data
    if last_val_patients is not None:
        for patient in last_val_patients:
            # print(' drop validation patients')
            patient.drop()  # drop dataframe in pandas?

   # generate patient-objects
    patients_test = []  # 空list
    patients_val = []
    patients_train = []

    for test_path_dcm, test_path_nii in zip(test_paths_dcm, test_paths_nii):
        patient = Patient(test_path_dcm, test_path_nii, forget_slices=forget_slices)
        patients_test.append(patient)
    for val_path_dcm, val_path_nii in zip(val_paths_dcm, val_paths_nii):
        patient = Patient(val_path_dcm, val_path_nii, forget_slices=forget_slices)
        patients_val.append(patient)
    for train_path_dcm, train_path_nii in zip(train_paths_dcm, train_paths_nii):
        patient = Patient(train_path_dcm, train_path_nii, forget_slices=forget_slices)
        patients_train.append(patient)
    print("done")
    # get slices for validation data
    patients_val_slices = []
    for patient in patients_val:
        slices = patient.get_slices(verbose=verbose)
        patients_val_slices.append(slices)

    patients_train_slices = []
    for patient in patients_train:
        slices = patient.get_slices(verbose=verbose)
        patients_train_slices.append(slices)


    #return (test_paths_dcm, test_paths_nii, train_paths_dcm, train_paths_nii, val_paths_dcm, val_paths_nii)
    return (patients_test, patients_train, patients_val, patients_val_slices,patients_train_slices)