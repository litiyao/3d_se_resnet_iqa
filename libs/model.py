# model test
import os
#os.environ["CUDA_DEVICE_ORDER"]="0000:02:00.0"


from tensorflow.python.client import device_lib
print(device_lib.list_local_devices)

import tensorflow as tf
import os.path
import scipy.io as sio
import numpy as np
import math
import keras
from keras.layers import Input
import keras.backend as K
from keras.layers import Conv2D, Conv3D,Dropout
from keras.layers import BatchNormalization
from keras.layers import GlobalAveragePooling2D
from keras.activations import softmax
from keras.layers import concatenate
from keras.layers.core import Dense, Activation, Flatten
from keras.models import Model
from keras.models import Sequential
from keras.layers import UpSampling3D
from keras.layers.convolutional import Convolution2D
from keras.layers import LeakyReLU
from keras.layers import Softmax
from keras.callbacks import EarlyStopping
from keras.callbacks import LearningRateScheduler
from keras.callbacks import ReduceLROnPlateau
from keras.callbacks import ModelCheckpoint
from keras.models import model_from_json
from keras.regularizers import l2  # , activity_l2

from keras.optimizers import SGD
from libs.deep_residual_learning_blocks import * #
#from matplotlib import pyplot as plt
from keras import regularizers
import scipy.io as sio


def createModel(patchSize, numClasses,feed_pos=True):

    if K.image_data_format() == 'channels_last':
        bn_axis = -1
    else:
        bn_axis = 1

    input_tensor = Input(shape=(patchSize[0], patchSize[1], patchSize[2], 1),name= 'input_X')

    if feed_pos:
        input_tensor_pos = Input(shape=(3,), name='input_position')


    # first stage
        #x = Conv3D(filters=16,kernel_size=(5, 5, 5),strides=(1, 1, 1),padding='same', kernel_initializer='he_normal',kernel_regularizer=regularizers.l2(0.01))(input_tensor)
    x = Conv3D(filters=16, kernel_size=(5, 5, 5), strides=(1, 1, 1), padding='same', kernel_initializer='he_normal')(input_tensor)
    #x = Conv3D(filters=16, kernel_size=(5, 5, 5), strides=(1, 1, 1), padding='same', kernel_initializer='glorot_normal')(input_tensor)
    x = BatchNormalization(axis=bn_axis)(x)
    x = LeakyReLU(alpha=0.01)(x)
    #x = Dropout(0.2)(x)
    x_after_stage_1 = Add()([input_tensor, x])

    # first down convolution
    x_down_conv_1 = projection_block_3D(x_after_stage_1,
                            filters=(32, 32),
                            kernel_size=(2, 2, 2),
                            stage=1,
                            block=1,
                            se_enabled=True,
                            se_ratio=4)

    # second stage
    x = identity_block_3D(x_down_conv_1, filters=(32, 32), kernel_size=(3, 3, 3), stage=2, block=1, se_enabled=True, se_ratio=4)
    #x_after_stage_2 = identity_block_3D(x, filters=(32, 32), kernel_size=(3,3,3), stage=2, block=2, se_enabled=True, se_ratio=4)
    x_after_stage_2 = x


    # second down convolution
    x_down_conv_2 = projection_block_3D(x_after_stage_2,
                                        filters=(64, 64),
                                        kernel_size=(2, 2, 2),
                                        stage=2,
                                        block=3,
                                        se_enabled=True,
                                        se_ratio=8)

    # third stage
    x = identity_block_3D(x_down_conv_2, filters=(64, 64), kernel_size=(3, 3, 3), stage=3, block=1, se_enabled=True, se_ratio=8)
    x_after_stage_3 = identity_block_3D(x, filters=(64, 64), kernel_size=(3, 3, 3), stage=3, block=2, se_enabled=True, se_ratio=8)
    #x = identity_block_3D(x, filters=(64, 64), kernel_size=(3, 3, 3), stage=3, block=3, se_enabled=False, se_ratio=16)

    # third down convolution
    x_down_conv_3 = projection_block_3D(x_after_stage_3,
                                        filters=(128, 128),
                                        kernel_size=(2, 2, 2),
                                        stage=3,
                                        block=4,
                                        se_enabled=True,
                                        se_ratio=16)

    # fourth stage
    x = identity_block_3D(x_down_conv_3, filters=(128, 128), kernel_size=(3, 3, 3), stage=4, block=1, se_enabled=True, se_ratio=16)
    x_after_stage_4 = identity_block_3D(x, filters=(128, 128), kernel_size=(3, 3, 3), stage=4, block=2, se_enabled=True, se_ratio=16)
    #x = identity_block_3D(x, filters=(128, 128), kernel_size=(3, 3, 3), stage=4, block=3, se_enabled=False, se_ratio=16)'''



    ### end of encoder path


    # use x_after_stage_4 as quantification output
    # global average pooling
    x_class = GlobalAveragePooling3D(data_format=K.image_data_format())(x_after_stage_4)

    #x_class = Dropout(0.5)(x_class)
    # fully-connected layer
    classification_output = Dense(units=numClasses,
                        activation='softmax',
                        kernel_initializer='he_normal',
                        name='output_Y')(x_class)



    # create model
    if feed_pos:
        cnn = Model(inputs=[input_tensor,input_tensor_pos], outputs=[classification_output], name='3D-SE-ResNet')
        sModelName = cnn.name
    else:
        cnn = Model(inputs =[input_tensor], outputs=[classification_output], name='3D-SE-ResNet')
        sModelName = cnn.name


    return cnn, sModelName

'''patchSize = (32,32,32)
numClasses = 3
cnn, sModelName = createModel(patchSize, numClasses)

def fTrainInner(cnn, modelName, X_train=None, y_train=None, X_valid=None, y_valid=None,
                 X_test=None, y_test=None, sOutPath=None,
                batchSize=None, learningRate=None, iEpochs=None):
    print('Training CNN')
    print('with lr = ' + str(learningRate) + ' , batchSize = ' + str(batchSize))

    # sio.savemat('D:med_data/' + 'checkdata_voxel_and_mask.mat',
    #             {'mask_train': Y_segMasks_train,
    #              'voxel_train': X_train,
    #              'mask_test': Y_segMasks_test,
    #              'voxel_test': X_test})

    # save names
    _, sPath = os.path.splitdrive(sOutPath)
    sPath, sFilename = os.path.split(sPath)
    sFilename, sExt = os.path.splitext(sFilename)

    model_name = sOutPath + os.sep + sFilename
    weight_name = model_name + '_weights.h5'
    model_json = model_name + '.json'
    model_all = model_name + '_model.h5'
    model_mat = model_name + '.mat'

    if (os.path.isfile(model_mat)):  # no training if output file exists
        print('------- already trained -> go to next')
        return

    # create optimizer

    opti = keras.optimizers.Adam(lr=0.001, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.0)

    cnn.summary()

    # compile model

    cnn.compile(loss='categorical_crossentropy', optimizer=opti, metrics= ['accuracy'])

    # callbacks
    #callback_earlyStopping = EarlyStopping(monitor='val_loss', patience=12, verbose=1)

    # callback_tensorBoard = keras.callbacks.TensorBoard(log_dir=dlart_handle.getLearningOutputPath() + '/logs',
    # histogram_freq=2,
    # batch_size=batchSize,
    # write_graph=True,
    # write_grads=True,
    # write_images=True,
    # embeddings_freq=0,
    # embeddings_layer_names=None,
    #  embeddings_metadata=None)

    #callbacks = [callback_earlyStopping]
    callbacks = []
    #callbacks.append(
     #   ModelCheckpoint(sOutPath + os.sep + 'checkpoints' + os.sep + 'checker.hdf5', monitor='val_acc', verbose=0,
      #                  period=1, save_best_only=True))  # overrides the last checkpoint, its just for security
    # callbacks.append(ReduceLROnPlateau(monitor='loss', factor=0.1, patience=5, min_lr=1e-4, verbose=1))
    callbacks.append(LearningRateScheduler(schedule=step_decay, verbose=1))

    if X_valid.size == 0 and y_valid.size == 0:
        # using test set for validation
        if usingClassification:
            result = cnn.fit(X_train,
                             {'segmentation_output': Y_segMasks_train, 'classification_output': y_train},
                             validation_split=0.15,
                             epochs=iEpochs,
                             batch_size=batchSize,
                             callbacks=callbacks,
                             verbose=1)
        else:
            result = cnn.fit(X_train,
                             Y_segMasks_train,
                             validation_split=0.15,
                             epochs=iEpochs,
                             batch_size=batchSize,
                             callbacks=callbacks,
                             verbose=1)
    else:
        # using validation set for validation
        if usingClassification:
            result = cnn.fit(X_train,
                             {'segmentation_output': Y_segMasks_train, 'classification_output': y_train},
                             validation_split=0.15,
                             epochs=iEpochs,
                             batch_size=batchSize,
                             callbacks=callbacks,
                             verbose=1)
        else:
            result = cnn.fit(X_train,
                             Y_segMasks_train,
                             validation_split=0.15,
                             epochs=iEpochs,
                             batch_size=batchSize,
                             callbacks=callbacks,
                             verbose=1)

    # return the loss value and metrics values for the model in test mode
    if usingClassification:
        model_metrics = cnn.metrics_names
        loss_test, segmentation_output_loss_test, classification_output_loss_test, segmentation_output_dice_coef_test, classification_output_acc_test \
            = cnn.evaluate(X_test, {'segmentation_output': Y_segMasks_test, 'classification_output': y_test}, batch_size=batchSize, verbose=1)
    else:
        score_test, dice_coef_test = cnn.evaluate(X_test, Y_segMasks_test, batch_size=batchSize, verbose=1)

    prob_test = cnn.predict(X_test, batchSize, 0)

    # save model
    json_string = cnn.to_json()
    with open(model_json, 'w') as jsonFile:
        jsonFile.write(json_string)

    # wei = cnn.get_weights()
    cnn.save_weights(weight_name, overwrite=True)
    # cnn.save(model_all) # keras > v0.7

    if not usingClassification:
        # matlab
        dice_coef_training = result.history['dice_coef']
        training_loss = result.history['loss']
        val_dice_coef = result.history['val_dice_coef']
        val_loss = result.history['val_loss']

        print('Saving results: ' + model_name)

        sio.savemat(model_name, {'model_settings': model_json,
                                 'model': model_all,
                                 'weights': weight_name,
                                 'dice_coef': dice_coef_training,
                                 'training_loss': training_loss,
                                 'val_dice_coef': val_dice_coef,
                                 'val_loss': val_loss,
                                 'score_test': score_test,
                                 'dice_coef_test': dice_coef_test,
                                 'prob_test': prob_test})
    else:
        # matlab
        segmentation_output_loss_training = result.history['segmentation_output_loss']
        classification_output_loss_training = result.history['classification_output_loss']
        segmentation_output_dice_coef_training = result.history['segmentation_output_dice_coef']
        classification_output_acc_training = result.history['classification_output_acc']

        val_segmentation_output_loss = result.history['val_segmentation_output_loss']
        val_classification_output_loss = result.history['val_classification_output_loss']
        val_segmentation_output_dice_coef = result.history['val_segmentation_output_dice_coef']
        val_classification_output_acc = result.history['val_classification_output_acc']

        print('Saving results: ' + model_name)

        sio.savemat(model_name, {'model_settings': model_json,
                                 'model': model_all,
                                 'weights': weight_name,
                                 'segmentation_output_loss_training': segmentation_output_loss_training,
                                 'classification_output_loss_training': classification_output_loss_training,
                                 'segmentation_output_dice_coef_training': segmentation_output_dice_coef_training,
                                 'classification_output_acc_training': classification_output_acc_training,
                                 'segmentation_output_loss_val': val_segmentation_output_loss,
                                 'classification_output_loss_val': val_classification_output_loss,
                                 'segmentation_output_dice_coef_val': val_segmentation_output_dice_coef,
                                 'classification_output_acc_val': val_classification_output_acc,
                                 'loss_test': loss_test,
                                 'segmentation_output_loss_test': segmentation_output_loss_test,
                                 'classification_output_loss_test': classification_output_loss_test,
                                 'segmentation_output_dice_coef_test': segmentation_output_dice_coef_test,
                                 'classification_output_acc_test': classification_output_acc_test,
                                 'segmentation_predictions': prob_test[0],
                                 'classification_predictions': prob_test[1]})'''

