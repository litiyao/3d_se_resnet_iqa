import numpy as np
import pandas as pd
import pandas
import ntpath
import os
from  libs.utilities import * #
from libs.patching import fRigidPatching3D


def crop3D(arr, pos, size): # array 4D某个病人的 pos：随机选取的点 pos = (98, 173, 188) size 32
    """Crop 3D out of images."""
    pos = np.array(pos)   #array([ 98, 173, 188])
    end = pos + np.array([size,size,size]) # 各加32 array([130, 205, 220])
    if (np.array(arr.shape[:3])-end).min() >= 0:
        result = arr[pos[0]:end[0], pos[1]:end[1], pos[2]:end[2] ,:]
    else:
        print('Invalid arguments, returning None.')
        result = None
    return result

def augmentation(dim, patient, label, samples_size, cropsize_X, border):
    """Data augmentation for training samples."""
    size_x, size_y, size_z = patient.shape[:3]  # 260 320 316
    pos_x_X = np.random.randint(border, size_x - cropsize_X - border,
                                samples_size)  # np.random.randint(20, 260-32-20, 1) low 20 high 208 size 1
    pos_y_X = np.random.randint(border, size_y - cropsize_X - border,
                                samples_size)  # np.random.randint(20, 320-32-20, 1) low 20 high 208 size 1
    pos_z_X = np.random.randint(border, size_z - cropsize_X - border,
                                samples_size)  # np.random.randint(20, 316-32-20, 1) low 20 high 208 size 1
    shape_X1 = (samples_size,) + dim * (cropsize_X,)  # shape_X1 (1, 32, 32, 32)




    channels = patient.shape[-1]  # 2
    shape_X2 = shape_X1 + (channels,)  # (1, 32, 32, 32, 2)
    samples_X = np.ndarray(shape_X2)  # 上面维度的array


    samples_Y = np.ndarray(samples_size)
    for i, pos in enumerate(zip(pos_x_X, pos_y_X, pos_z_X)):
        samples_X[i, ...] = crop3D(patient, pos,
                                 cropsize_X)  # 最重要的点 (3, 32, 32, 32, 2) 3 代表sample_size  samples_X_0 =samples_X[0,...] 取第0个, 它的shape是 (32, 32, 32, 2)

    for i in range(samples_size):
        samples_Y[i] = label

    pos_X = [np.array([pos_x, pos_y, pos_z]) for pos_x, pos_y, pos_z in
             zip(pos_x_X, pos_y_X, pos_z_X)]  # 选出的 sample_size 个顶点 [ 98, 203, 141] [ 24, 158, 219] [157, 142,  39]
    #  normalize position
    max_pos = np.array([size_x - cropsize_X - border, size_y - cropsize_X - border,
                        size_z - cropsize_X - border])  # 顶点可以取的最大值 [208, 268, 264]
    pos_X = [np.divide(pos, max_pos) for pos in pos_X]  # 分别除以顶点最大值 #98/208 203/268 141/264 <class 'list'>: [array([0.40865385, 0.7761194 , 0.56439394]), array([0.75, 0.52238806, 0.72348485]), array([0.875, 0.22014925, 0.95833333])]

    return samples_X, pos_X, samples_Y


'''dim = 3
patient_ob = Patient("D:/2channel_array_with_norm/100063_30.npy",'C:/Users/litiy/PycharmProjects/FCN_classification_Tiyao/CombiExcel.xlsx', forget_slices=False)
patient = patient_ob.load_slices(verbose=False)[0]
label = patient_ob.load_slices(verbose=False)[1]
samples_size =3
cropsize_X = 32
border = 20
samples_X, pos_X, samples_Y = augmentation(dim, patient, label, samples_size, cropsize_X, border)'''


def get_meshgrid(patient, density, cropsize, border):
    """Get meshgrid to generate validation samples."""
    x = np.linspace(border, patient.shape[0] - cropsize - border, density) # 20 ~208 pick 5(density) numbers with  the same distance array([ 20.,  67., 114., 161., 208.])
    y = np.linspace(border, patient.shape[1] - cropsize - border, density)  # 20~ 268 array([ 20.,  82., 144., 206., 268.])
    z = np.linspace(border, patient.shape[2] - cropsize - border, density) # 20~ 264  array([ 20.,  81., 142., 203., 264.])

    result = (np.array(np.meshgrid(x, y, z)).reshape(3, density ** 3).T).astype('int')
    return result
def get_valid(mode, patients_valid, labels_valid, border, patchSize, patchOverlap):
    """Get validation data."""
    X_valid = []
    Y_valid = []
    dim = int(mode[0]) #3
    all_patches_list = []
    all_labels_list = []

    for i, (patient, label) in enumerate(zip(patients_valid, labels_valid)):
        dicom_numpy_array = patient[:,:,:,0]
        dicom_numpy_array = dicom_numpy_array[border:(dicom_numpy_array.shape[0] - border),
                            border:(dicom_numpy_array.shape[1] - border), border:(dicom_numpy_array.shape[2] - border)]
        dPatches, dLabels = fRigidPatching3D(dicom_numpy_array, patchSize, patchOverlap, label)
        dPatches = np.transpose(dPatches, (3, 0, 1, 2))
        all_patches_list.append(dPatches)
        all_labels_list.append(dLabels)

    nbPatches = all_patches_list[0].shape[0]  # 120
    length = nbPatches * len(labels_valid)
    X_valid = np.zeros((length, patchSize[0], patchSize[1], patchSize[2], 1), dtype='float16')

    nb = 0
    for dPatches in all_patches_list:
        dPatches_4D = np.zeros((nbPatches, patchSize[0], patchSize[1], patchSize[2], 1), dtype='float16')
        dPatches_4D[:, :, :, :, 0] = dPatches
        X_valid[(nb * nbPatches):((nb + 1) * nbPatches), :, :, :, :] = dPatches_4D
        nb = nb + 1
    labels_list = []
    for dLabels in all_labels_list:
        labels_list = labels_list + dLabels

    Y_valid = np.array(labels_list)

    return X_valid, Y_valid

def get_train(mode, patients_train, labels_train, border, patchSize, patchOverlap):
    """Get validation data."""
    X_train = []
    Y_train = []
    dim = int(mode[0]) #3
    all_patches_list = []
    all_labels_list = []
    for i, (patient, label) in enumerate(zip(patients_train, labels_train)):
        dicom_numpy_array = patient[:,:,:,0]
        dicom_numpy_array = dicom_numpy_array[border:(dicom_numpy_array.shape[0] - border),
                            border:(dicom_numpy_array.shape[1] - border), border:(dicom_numpy_array.shape[2] - border)]
        dPatches, dLabels = fRigidPatching3D(dicom_numpy_array, patchSize, patchOverlap, label)
        dPatches = np.transpose(dPatches, (3, 0, 1, 2))
        all_patches_list.append(dPatches)
        all_labels_list.append(dLabels)



    '''for i, (patient, label) in enumerate(zip(patients_train, labels_train)):
        positions_X = get_meshgrid(patient, density, cropsize_X, border) # 获取采样点 顶点 125 个

        # normalize postion
        size_x, size_y, size_z = patient.shape[:3]
        max_pos = np.array([size_x-cropsize_X-border, size_y-cropsize_X-border, size_z-cropsize_X-border])
        positions_X_normalized = [np.divide(pos_X, max_pos) for pos_X in positions_X]

        pos_feed.append(positions_X_normalized) # 标准化后的采样点位置



        for pos_X in positions_X:
            X_train.append(crop3D(patient, pos_X, cropsize_X)) #for every patient get 125(5*5*5) cubes with dimension 32*32*32*2
            Y_train.append(label) # or every patient 125 same labels


    X_valid = np.array(X_train) # 125 same labels list to array
    Y_valid = np.array(Y_train) # 125(5*5*5) cubes list to array

    pos_feed = np.reshape(np.array(pos_feed), (-1,3))'''
    nbPatches = all_patches_list[0].shape[0] #120
    length = nbPatches*len(labels_train)
    X_train = np.zeros((length, patchSize[0], patchSize[1], patchSize[2],1), dtype='float16')

    nb=0
    for dPatches in all_patches_list:
        dPatches_4D = np.zeros((nbPatches,patchSize[0], patchSize[1], patchSize[2], 1), dtype='float16')
        dPatches_4D[:,:,:,:,0] = dPatches
        X_train[(nb*nbPatches):((nb+1)*nbPatches),:,:,:,:] = dPatches_4D
        nb=nb+1
    labels_list = []#

    '''with open('unreformed_label.p', 'wb') as f:
        pickle.dump(all_labels_list, f)'''

    for dLabels in all_labels_list:

        labels_list = labels_list + dLabels

    Y_train = np.array(labels_list)

    return X_train, Y_train