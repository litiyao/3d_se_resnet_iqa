import random
import numpy as np
import keras
import libs.utilities
import libs.preprocessing
import pickle
from sklearn.utils import shuffle


class PatientBuffer():
    """Loading patients dynamicly in RAM while training."""

    def __init__(self, patients, capacity, batch_size, cropsize_X, dim, verbose=False):
        """Constructor."""
        self.patients = patients
        self.batch_size = batch_size
        self.cropsize_X = cropsize_X
        self.dim = dim
        self.verbose = verbose

        if len(patients) < capacity:
            capacity = len(patients)  # 可以放入的病人数量 amount of patients on RAM 30

        self.samples_per_patient = batch_size // capacity  # 48  //30  = 1
        self.sh_register = utilities.ShiftRegister(capacity=capacity)  # ShiftRegister 的类
        self.patient_pointer = capacity  # 30

        for patient in self.patients[:capacity]:  # patients 是列表 列表里的元素是 每个patient对象
            self.sh_register.shift(patient.get_slices(verbose=verbose),
                                   verbose=True)  # new data = patient.get_slices(verbose=verbose) = (ArrayDicom, Label)  读入new data 作为 content, 指针向后移动1

        self.build_batch_buffer()

    def shift(self):
        """Fill shift-register of PatientBuffer."""
        capacity = self.sh_register.capacity

        if self.patient_pointer == capacity - 1:
            for i in range(capacity - 1):
                self.patients[capacity - i - 2].drop()
            self.patients[len(self.patients) - 1].drop()
            # random.shuffle(self.patients)
            for patient in self.patients[:capacity]:
                self.sh_register.shift(patient.get_slices(verbose=self.verbose), verbose=True)
        else:
            self.sh_register.shift(self.patients[self.patient_pointer].get_slices(verbose=self.verbose))
            self.patients[self.patient_pointer - capacity].drop()

        self.patient_pointer = (self.patient_pointer + 1) % len(self.patients)

    def drop_all(self):
        for i in range(len(self.patients)):
            self.patients[i].drop()

    def build_batch_buffer(self):
        channels = 2  # 从content 获取channel数
        shape_X = (self.batch_size,) + self.dim * (self.cropsize_X,) + (channels,)  # (batch_size, 32,32,32,2) 48
        shape_Y = (self.batch_size,)

        self.batch_X = np.ndarray(shape_X)
        self.batch_Y = np.ndarray(shape_Y)

    def sample_batch(self, border):
        positions = []
        end = 0
        for patient_train_slices, label_train_slices in self.sh_register.content:
            start = end
            end += self.samples_per_patient
            self.batch_X[start:end, ...], pos, self.batch_Y[start:end, ...] = preprocessing_overlap.augmentation(self.dim,
                                                                                                         patient_train_slices,
                                                                                                         label_train_slices,
                                                                                                         self.samples_per_patient,
                                                                                                         self.cropsize_X,
                                                                                                         border)
            positions += pos

        for i in range(end, self.batch_size):
            rnd = np.random.randint(self.sh_register.capacity)
            patient_train_slices, label_train_slices = [arr for arr in self.sh_register.content[rnd]]
            self.batch_X[i, ...], pos, self.batch_Y[i, ...] = preprocessing_overlap.augmentation(self.dim,
                                                                                         patient_train_slices,
                                                                                         label_train_slices,
                                                                                         1,
                                                                                         self.cropsize_X,
                                                                                         border)
            positions += pos
        positions = np.array(positions)
        return (self.batch_X, positions, self.batch_Y)
        # batch_X 是  (48, 32, 32, 32, 2) 的array 代表选取的48个cube 双通道
        # positons 是 (48, 3) array，表示 48 个顶点相对位置
        # batch_Y 是单维度array， 每个值对应 label [1. 1. 1. 1. 2. 2. 2. 2. 1. 1. 1. 1. 1. 1. 1. 1. 2. 2. 2. 2. 1. 1. 1. 1.
        #  1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 2. 1. 1. 1.]


'''def generator_train(mode,
                    patients_train,
                    patient_buffer_capacity,
                    batches_per_shift,
                    batch_size,
                    cropsize_X,
                    border,
                    mult_inputs,
                    epochs,
                    steps_per_epoch,
                    empty_patient_buffer):
    """Generater for feeding training batches to GPU."""
    patient_buffer = PatientBuffer(patients=patients_train,
                                   capacity=patient_buffer_capacity,
                                   batch_size=batch_size,
                                   cropsize_X=cropsize_X,
                                   dim=int(mode[0]))
    counter = 0
    batch_counter = 0
    final = steps_per_epoch * epochs
    while True:
        batch_X, pos, batch_Y = patient_buffer.sample_batch(border=border)
        # batch_X 是  (48, 32, 32, 32, 2) 的array 代表选取的48个cube 双通道
        # positons 是 (48, 3) array，表示 48 个顶点相对位置
        # batch_Y 是单维度array， 每个值对应 label [1. 1. 1. 1. 2. 2. 2. 2. 1. 1. 1. 1. 1. 1. 1. 1. 2. 2. 2. 2. 1. 1. 1. 1.
        #  1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 2. 1. 1. 1.]
        batch_Y = batch_Y.tolist()
        list = []
        for label in batch_Y:
            label = label - 1
            label = keras.utils.np_utils.to_categorical(label, 3)
            list.append(label)
        batch_Y = np.asarray(list) #48*2 array

        counter += 1
        if ((len(patients_train) > patient_buffer_capacity) and (counter == batches_per_shift)):  # 如果训练病人数大于可以加载的病人数 and
            counter = 0
            patient_buffer.shift()

        batch_X_dict = {'input_X': batch_X}
        batch_Y_dict = {'output_Y': batch_Y}
        if mult_inputs:
            batch_X_dict['input_position'] = pos

        if empty_patient_buffer:
            batch_counter += 1
            if batch_counter == final:
                # print("Drop all!")
                patient_buffer.drop_all()

        yield batch_X_dict, batch_Y_dict'''


def generator_train(X_train, Y_train, batch_size):
    """Generater for feeding validation batches to GPU."""
    size = Y_train.shape[0]  # 125*len(valid) 3750
    limit = size - batch_size  # 125*len(valid)-48
    end = size + 1  # 125*len(valid)+1
    while True:
        if end > limit:
            end = 0
        start = end
        end += batch_size
        batch_X = X_train[start:end]  # 0~48 48~96
        batch_Y = Y_train[start:end]

        batch_Y = batch_Y.tolist()
        list = []
        for label in batch_Y:
            label = label - 1
            label = keras.utils.np_utils.to_categorical(label, 3)
            list.append(label)
        batch_Y = np.asarray(list)

        batch_X_dict = {'input_X': batch_X}
        batch_Y_dict = {'output_Y': batch_Y}


        yield batch_X_dict, batch_Y_dict


def generator_valid(X_valid, Y_valid, batch_size):
    """Generater for feeding validation batches to GPU."""
    size = Y_valid.shape[0]  # 125*len(valid) 3750
    limit = size - batch_size  # 125*len(valid)-48
    end = size + 1  # 125*len(valid)+1
    while True:
        if end > limit:
            end = 0
        start = end
        end += batch_size
        batch_X = X_valid[start:end]  # 0~48 48~96
        batch_Y = Y_valid[start:end]

        batch_Y = batch_Y.tolist()
        list = []
        for label in batch_Y:
            label = label - 1
            label = keras.utils.np_utils.to_categorical(label, 3)
            list.append(label)
        batch_Y = np.asarray(list)

        batch_X_dict = {'input_X': batch_X}
        batch_Y_dict = {'output_Y': batch_Y}



        yield batch_X_dict, batch_Y_dict


def fit(model,
              data_valid,
              data_train,
              epochs,
              batch_size,
              patchSize,
              patchOverlap,
              border,
              callbacks):
    """Perform training on given model and datasets."""



    cropsize_X = model.get_input_shape_at(0)[1]

    dim = 3
    mode = str(dim) + 'D'

    patients_train = []
    labels_train = []
    for patient_train, label_train in data_train:
        patients_train.append(patient_train)
        labels_train.append(label_train)

    patients_valid = []
    labels_valid = []
    for patient_valid, label_valid in data_valid:
        patients_valid.append(patient_valid)
        labels_valid.append(label_valid)

    '''gen_train = generator_train(mode=mode,  # 3D
                                patients_train=patients_train,  # 对象集合
                                patient_buffer_capacity=patient_buffer_capacity,  # 30
                                batches_per_shift=batches_per_shift,  # 25
                                batch_size=batch_size,  # 48
                                cropsize_X=cropsize_X,  # 32
                                border=border,  # 20
                                mult_inputs=mult_inputs,  # True
                                epochs=epochs,  # 50
                                steps_per_epoch=batches_per_train_epoch,
                                empty_patient_buffer=empty_patient_buffer)  # True
    gen_train_0 = next(gen_train)'''

    '''with open('gen_train_0.pickle', 'wb') as handle:
            pickle.dump(gen_train_0, handle, protocol=pickle.HIGHEST_PROTOCOL)
    print('gen_train_0.pickle saved')
    data = gen_train_0
    if isinstance(data, dict):
        try:
            data = [data[x].values if data[x].__class__.__name__ == 'DataFrame' else data[x] for x in names]
        except KeyError as e:
            raise ValueError(
                'No data provided for "' + e.args[0] + '". Need data '
                                                       'for each key in: ' + str(names))'''

    X_train,  Y_train = libs.preprocessing.get_train(mode=mode,
                                                            patients_train=patients_train,
                                                            labels_train=labels_train,
                                                            patchSize = patchSize,
                                                            patchOverlap = patchOverlap,
                                                            border=border)

    X_train,Y_train = shuffle(X_train, Y_train)

    size = Y_train.shape[0]
    batch_X = X_train[0:size]  # 0~48 48~96
    batch_Y = Y_train[0:size]
    batch_Y = batch_Y.tolist()
    list = []
    for label in batch_Y:
        label = label - 1
        label = keras.utils.np_utils.to_categorical(label, 3)
        list.append(label)
    batch_Y = np.asarray(list)

    batch_X_dict = {'input_X': batch_X}
    batch_Y_dict = {'output_Y': batch_Y}

    print("fit starts")

    histObj = model.fit(x=batch_X_dict,
                        y=batch_Y_dict,
                        batch_size=batch_size,
                        epochs=epochs,
                        validation_split=0.3,
                        shuffle=True,
                        callbacks=callbacks)

    return histObj

    '''gen_train = generator_train(X_train=X_train,
                                Y_train=Y_train,
                                batch_size=batch_size
                                )



    X_valid, Y_valid = preprocessing_overlap.get_valid(mode=mode,
                                                            patients_valid=patients_valid,
                                                            labels_valid=labels_valid,
                                                            patchSize=patchSize,
                                                            patchOverlap = patchOverlap,
                                                            border=border)

    gen_valid = generator_valid(X_valid=X_valid,
                                Y_valid=Y_valid,
                                batch_size=batch_size)

    gen_valid_0 = next(gen_valid)

    steps_valid = Y_valid.shape[0] // batch_size

    print("fit starts")

    batches_per_train_epoch = len(Y_train) // batch_size + 1

    histObj = model.fit_generator(generator=gen_train,
                                  epochs=epochs,
                                  steps_per_epoch=batches_per_train_epoch,
                                  validation_data=gen_valid,
                                  validation_steps=steps_valid,
                                  max_q_size= 50,
                                  callbacks=callbacks)
    return histObj'''




