"""
@author: Sebastian Milde, Thomas Kuestner
"""
import numpy as np
import math


def fRigidPatching3D(dicom_numpy_array, patchSize, patchOverlap,label):


    dOverlap = np.multiply(patchSize, patchOverlap)
    dNotOverlap = np.ceil(np.multiply(patchSize, (1 - patchOverlap)))

    size_zero_pad = np.array(
        [math.ceil((dicom_numpy_array.shape[0] - dOverlap[0]) / (dNotOverlap[0])) * dNotOverlap[0] + dOverlap[
            0], math.ceil((dicom_numpy_array.shape[1] - dOverlap[1]) / (dNotOverlap[1])) * dNotOverlap[1] + dOverlap[1],
         math.ceil((dicom_numpy_array.shape[2] - dOverlap[2]) / (dNotOverlap[2])) * dNotOverlap[2] + dOverlap[2]])
    zero_pad = np.array([int(math.ceil(size_zero_pad[0])) - dicom_numpy_array.shape[0],
                         int(math.ceil(size_zero_pad[1])) - dicom_numpy_array.shape[1],
                         int(math.ceil(size_zero_pad[2])) - dicom_numpy_array.shape[2]])
    zero_pad_part = np.array(
        [int(math.ceil(zero_pad[0] / 2)), int(math.ceil(zero_pad[1] / 2)), int(math.ceil(zero_pad[2] / 2))])

    Img_zero_pad = np.lib.pad(dicom_numpy_array, ((zero_pad_part[0], zero_pad[0] - zero_pad_part[0]), (zero_pad_part[1], zero_pad[1] - zero_pad_part[1]),(zero_pad_part[2], zero_pad[2] - zero_pad_part[2])),
                              mode='constant')

    nbPatches = ((size_zero_pad[0] - patchSize[0]) / ((1 - patchOverlap) * patchSize[0]) + 1) * (
    (size_zero_pad[1] - patchSize[1]) / ((1 - patchOverlap) * patchSize[1]) + 1) * (
                (size_zero_pad[2] - patchSize[2]) / (np.round((1 - patchOverlap) * patchSize[2])) + 1)
    dPatches = np.zeros((patchSize[0], patchSize[1], patchSize[2], int(nbPatches)), dtype='float16')


    idxPatch = 0
    for iZ in range(0, int(size_zero_pad[2] - dOverlap[2]), int(dNotOverlap[2])):
        for iY in range(0, int(size_zero_pad[0] - dOverlap[0]), int(dNotOverlap[0])):
            for iX in range(0, int(size_zero_pad[1] - dOverlap[1]), int(dNotOverlap[1])):
                dPatch = Img_zero_pad[iY:iY + patchSize[0], iX:iX + patchSize[1], iZ:iZ + patchSize[2]]
                dPatches[:, :, :, idxPatch] = dPatch
                idxPatch += 1


    dPatches = dPatches[:, :, :, 0:idxPatch]
    dLabels = int(idxPatch) * [label]



    return dPatches, dLabels

