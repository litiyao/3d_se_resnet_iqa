import os
os.environ["CUDA_VISIBLE_DEVICES"]= "3"
import keras
import keras.backend as K
from keras import callbacks as cb
from keras.models import Sequential, Model
from keras.layers import *
K.set_image_data_format = 'channels_last'

# import own scripts
import libs.utilities
import libs.preprocessing
from libs.training import *
import libs.model
from keras.models import load_model
import os

import numpy as np
import pandas as pd
import pandas
import ntpath

import pickle

from libs.deep_residual_learning_blocks import * #

class Patient():
    """Creates patient."""

    def __init__(self, dicom_dirs, label_dirs, forget_slices=False):
        """"Constructor."""
        self.dicom_dirs = dicom_dirs  # dicom array path
        self.label_dirs = label_dirs # 已经是 1,2,3,
        self.slices = None
        self.shape = None
        self.prediction = None
        self.forget_slices = forget_slices
        self.slice_counter = 0

    def get_slices(self, count=True, verbose=False):
        """Take loaded slices if already available or load slices if not."""
        if count:  # 如果count是true， 计数加一
            self.slice_counter += 1
        if self.slices is None:
            self.slices = self.load_slices(verbose=verbose)
            # print("slice-counter: " + str(self.slice_counter))
            # print("load slices!")
        return self.slices

    def drop(self):
        """Drop slices for PatientBuffer in training."""
        # if self.slices == None:
        # print("Slices are already None!")
        if self.slice_counter != 0:  # 如果slice计数不等于0，计数减去1
            self.slice_counter -= 1
            if self.slice_counter == 0 and self.forget_slices:
                self.slices = None

    def load_slices(self, verbose=False):
        """Load patients data (dicom- and nii-files) in numpy arrays."""
        ArrayDicom = np.load(self.dicom_dirs)
        label =  self.label_dirs

        '''basename = os.path.basename("D:/2channel_array_with_norm/100063_30.npy")
        basename = os.path.basename(self.dicom_dirs)
        patientID = basename[:9]

        Label_dataframe = pd.read_excel(self.label_dirs)
        #Label_dataframe = pd.read_excel('C:/Users/litiy/PycharmProjects/FCN_classification_Tiyao/CombiExcel.xlsx')
        patientID_column = Label_dataframe['PatientID_FolderName']
        NumberOfRowsNE = len(Label_dataframe.index)
        for RowInNE in range(NumberOfRowsNE):
            row1 = patientID_column.iloc[RowInNE]
            if row1 == patientID:
                Label = Label_dataframe.at[RowInNE, 'QualityRating']
                #print(RowInNE) '''

        return (ArrayDicom, label)


# paths to data (patients)
path= "/home/d1259/no_backup/d1259/FCN_breath/"

# path were to save the model and history
path_m = "/home/d1259/no_backup/d1259/FCN_breath/models/"
path_h = "/home/d1259/no_backup/d1259/FCN_breath/histories/"

# list for history-objects
lhist = []

model = load_model('/home/d1259/no_backup/d1259/3D_SE_ResNet_IQA/models/epoch_70_batchsize_64_overlap_70_opti_SGD.hdf5')
#opti = keras.optimizers.Adam(lr=0.01, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.0)
opti = keras.optimizers.SGD(lr=0.1, momentum=0.9, decay=0.0001)
model.compile(loss=keras.losses.categorical_crossentropy, optimizer=opti, metrics=['accuracy'])

# k=4, 50% out of every 1/4-part, 45 patients discarded (as test-patients)
with open(os.path.join(path, 'patients.p'), 'rb') as patients:
    DicomPathsList = pickle.load(patients)  # dicom path list
with open(os.path.join(path, 'labels.p'), 'rb') as labels:  # NiiPathsList?
    NiiPathsList = pickle.load(labels)  # like txt better

patients_to_take = 9
test_paths_dcm = DicomPathsList[:patients_to_take]  # first patients_to_take
test_paths_nii = NiiPathsList[:patients_to_take]

patients_test = []

for test_path_dcm, test_path_nii in zip(test_paths_dcm, test_paths_nii):
    patient = Patient(test_path_dcm, test_path_nii, forget_slices=True)
    patients_test.append(patient)

patients_test_slices = []
for patient in patients_test:
    slices = patient.get_slices(verbose=True)
    patients_test_slices.append(slices)


patients_test = []
labels_test = []
for patient_train, label_test in patients_test_slices:
    patients_test.append(patient_train)
    labels_test.append(label_test)


X_test, Y_test = libs.preprocessing.get_train(mode='3D',
                                                         patients_train=patients_test,
                                                         labels_train=labels_test,
                                                         border=40,
                                                         patchSize = (64,64,64),
                                                         patchOverlap=0.7)
batch_X = X_test[0:X_test.shape[0]] # 0~48 48~96
batch_Y = Y_test[0:Y_test.shape[0]]
batch_Y = batch_Y.tolist()
list = []
for label in batch_Y:
    label = label - 1
    label = keras.utils.np_utils.to_categorical(label, 3)
    list.append(label)
batch_Y = np.asarray(list)

batch_X_dict = {'input_X': batch_X}
batch_Y_dict = {'output_Y': batch_Y}

score, acc = model.evaluate(batch_X_dict, batch_Y_dict,
                            batch_size= 64)
print('Test score:', score)
print('Test accuracy:', acc)