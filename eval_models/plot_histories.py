import numpy as np
import pickle

import matplotlib.pyplot as plt

def load_histories(path):
    with open(path, 'rb') as f:
        lhist = pickle.load(f)
    return lhist

lhist = load_histories('/home/d1259/no_backup/d1259/3D_SE_ResNet_IQA/histories/epoch_70_batchsize_64_overlap_70_opti_SGD.p')
history = lhist[0]

# summarize history for accuracy
plt.plot(history['acc'])
plt.plot(history['val_acc'])
plt.title('NAKO_IQA_acc')
plt.ylabel('accuracy')
plt.xlabel('epoch')
plt.legend(['train', 'val'], loc='upper left')
plt.show()

# summarize history for loss
plt.plot(history['loss'])
plt.plot(history['val_loss'])
plt.title('NAKO_IQA_loss')
plt.ylabel('loss')
plt.xlabel('epoch')
plt.legend(['train', 'val'], loc='upper left')
plt.show()

